Plot()
{
Curve("IntCal13","IntCal13.14c");
Sequence()
{
Boundary("Start occupation");
Phase()
{
R_date("HST Beta-149404",1130,40)    ;
R_date("HST SUERC-3429",1160,35)    ;
R_date("HST SUERC-3430",1170,40)    ;
R_date("HST SUERC-8618",1110,40)    ;
R_date("HST SUERC-8619",1110,30)    ;
R_date("HST SUERC-8623",1130,35)    ;
R_date("HST SUERC-8624 ",1080,35)    ;
R_date("HST Beta-124004",1170,40)    ;
R_date("HST SUERC-3433",1030,35)    ;
R_date("HST SUERC-3432",1040,40)    ;
R_date("HST Beta-149403",1120,40)    ;
};
Boundary();
};
};