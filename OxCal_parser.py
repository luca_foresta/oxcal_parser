"""
This program reads an excel (.xlsx) or comma separated value (.csv) file and generates
a text (.txt) file to be used as input for the OxCal program (https://c14.arch.ox.ac.uk/oxcal.html).
With this program it is possible to load more input fields than allowed by OxCal
(version 4.3) so that no manual input is needed in the OxCal text browser.
This program allows to automatically input more than the basic three fields (date ID, age, error).
This program has NOT been developed in collaboration with the OxCal developers.

Example: python OxCal_parser.py input_file.xlsx

Luca Foresta - luca.foresta@ed.ac.uk
University of Edinburgh
07/2017
"""

import pandas
import os
import sys
import glob

# Read excel sheet into pandas database
if not os.path.isdir(sys.argv[1]):
    file_list = []
    file_list.append(sys.argv[1])
else:
    try:
        file_list = glob.glob(sys.argv[1] + '/*xlsx')
    except:
        file_list = glob.glob(sys.argv[1] + '/*csv')

for this_file in file_list:

    filename0, ext = os.path.splitext(this_file)
    if ext == '.xlsx':
        df = pandas.read_excel(this_file)
    elif ext == '.csv':
        df = pandas.read_csv(this_file)

    # Read fields
    data_fields = ['Sample ID', 'Conventional Radiocarbon Age', 'Error', \
                   'Outlier Model', 'P Value', 'Color', \
                   'Start Boundary Label', 'Stratigraphic Block', 'Block Label', \
                   'Calibration Curve', 'Date Type', 'Stratigraphic Block 2', \
                   'Block Label 2']
    data = df[data_fields]

    # Get number of data entries
    N_rows = len(data['Sample ID'])

    # Get calibration curve
    cal_curve = str(data['Calibration Curve'].values[0])

    outlier_flags = pandas.isnull(data['Outlier Model'].values)
    # Check which outlier model(s) is specified, if any
    # Also check that for any specified outlier model, the sum of all pvalues is more than 0,
    # else that outlier model is not added to the program header
    outlier_types = data['Outlier Model'][outlier_flags==False].unique()
    outlier_vals_sum = {}
    for k, el in enumerate(outlier_types):
        outlier_types[k] = outlier_types[k].encode('ascii','ignore').lower()
        outlier_vals_sum[outlier_types[k]] = sum(data['P Value'].values[data['Outlier Model'].values==el])
    # Create empty outlier_types if nothing specified in spread sheet (else code breaks later)
    if len(outlier_types) == 0:
        outlier_types = ''

    filename = filename0 + '.txt'
    with open(filename,'w') as outfile:
        header_0 = 'Plot()' + '\n' + '{' + '\n'
        # Initialize models as empty
        models = ''
        # Add general model if specified in spread sheet
        if ('general' in outlier_types) and (outlier_vals_sum['general'] != 0):
            # Add general model for outliers
            models = 'Outlier_Model("General", T(5), U(0,4), "t");' + '\n'
        # Add charcoal model if specified in spread sheet
        if 'charcoal plus' in outlier_types and (outlier_vals_sum['charcoal plus'] != 0):
            models = models \
                     + 'Prior("charcoal_plus","charcoal_plus.prior");' + '\n' \
                     + 'Outlier_Model("Charcoal_Plus",Prior("charcoal_plus"),U(0,3),"t");' + '\n'
            
        header_1 = 'Curve("' + cal_curve + '","' + cal_curve + '.14c");' + '\n' \
                 + 'Sequence()' + '\n' \
                 + '{' + '\n'
        outfile.write(header_0 + models + header_1)

        # Check how many stratigraphic blocks exist in the dataset
        if pandas.isnull(data['Stratigraphic Block 2'].values[0]):
            data['Stratigraphic Block 2'] = 1
            data['Block Label 2'] = ''
        if pandas.isnull(data['Stratigraphic Block'].values[0]):
            # Initialize single stratigraphic phase if nothing else is
            # specified
            min_block = 1
            max_block = 1
            data['Stratigraphic Block'] = 1
            data['Block Label'] = 'Phase'
        min_block = int(min(data['Stratigraphic Block']))
        max_block = int(max(data['Stratigraphic Block']))

        # Add label to main boundary
        if not pandas.isnull(data['Start Boundary Label'].values[0]):
            start_boundary_label = '"' + data['Start Boundary Label'][0] + '"'
        else:
            start_boundary_label = ''
        header_2 = 'Boundary(' + start_boundary_label + ');' + '\n'
        outfile.write(header_2)

        # Range through all stratigraphic blocks (outer layer)
        for i in range(min_block,max_block+1):
            block_2 = data[(data['Stratigraphic Block']==i)]['Stratigraphic Block 2']
            min_block_2 = int(min(block_2))
            max_block_2 = int(max(block_2))

            # Range through all stratigraphic blocks (innner layer) of current stratigraphic block
            write_block_type_1 = True
            for j in range(min_block_2, max_block_2+1):
                if max_block_2>1:
                    write_block_type_2 = True
                else:
                    write_block_type_2 = False
                # Do not start sequence/phase if there is just one element
                # in current stratigraphic block
                this_block_entries = len(data[ \
                            (data['Stratigraphic Block']==i) & \
                            (data['Stratigraphic Block 2']==j)   \
                                ].values)
                #print this_block_entries
                if this_block_entries == 1:
                    write_block_type_1 = False
                    write_block_type_2 = False
                for item in data[ \
                            (data['Stratigraphic Block']==i) & \
                            (data['Stratigraphic Block 2']==j)   \
                                ].values:
                    if write_block_type_1:
                        if '(' not in item[8]:
                            item[8] = item[8] + '()'
                        block_type = item[8] + '\n' + '{' + '\n'
                        outfile.write(block_type)
                        write_block_type_1 = False

                    if write_block_type_2:
                        if '(' not in item[12]:
                            item[12] = item[12] + '()'
                        block_type = item[12] + '\n' + '{' + '\n'
                        outfile.write(block_type)
                        write_block_type_2 = False

                    # Select date type
                    if str(item[10]).lower() == 'radiocarbon':
                        date_type = 'R_date'
                    elif str(item[10]).lower() == 'calendar':
                        date_type = 'C_date'
                    # Create basic entry (date_type,date_ID,age,error)
                    basic_line = date_type + '("' + str(item[0]) + '",' + str(item[1]) + ',' + str(item[2]) + ')  '
                    # Add outlier info if specified
                    if not pandas.isnull(item[3]):
                        # Leave outlier empty if its p value is zero
                        if item[4]==0:
                            outlier_entry = ''
                        else:
                            outlier_entry = '"' + str(item[3]) + '",' + str(item[4])
                            outlier_line = 'Outlier(' + outlier_entry + ');'
                    else:
                        outlier_line = ''
                    # Add color info if specified
                    if not pandas.isnull(item[5]):
                        color_line = 'color="' + str(item[5]).lower() + '";'
                    else:
                        color_line = ''
                    # Add and close curly braces if outlier or color info is specified
                    if (outlier_line != '') or (color_line != ''):
                        open_curly_brace = '{';
                        close_curly_brace = '}';
                    else:
                        open_curly_brace = '';
                        close_curly_brace = '';
                    # Add basic closing line for this entry
                    basic_close = '  ;' + '\n'

                    cmd_string = basic_line + open_curly_brace + outlier_line + \
                                 color_line + close_curly_brace + basic_close
                                 
                    outfile.write(cmd_string)
                
                # Close inner stratigraphic block (if it was open)
                if (max_block_2 !=1) and (this_block_entries != 1):
                    cmd_string = '};' + '\n'
                else:
                    cmd_string = ''
                # Add innner boundary between stratigraphic blocks
                # if this is not the last inner block in the other block
                # and if it is not the only sub-block
                if (max_block_2 ==1) or (j == max_block_2):
                    boundary_line = ''
                else:
                    boundary_line = 'Boundary();' + '\n'
                cmd_string = cmd_string + boundary_line
                outfile.write(cmd_string)

            # Close outer stratigraphic block
            if this_block_entries != 1:
                cmd_string = '};' + '\n'
            else:
                cmd_string = ''
            cmd_string = cmd_string + 'Boundary();' + '\n'
            outfile.write(cmd_string)

        # Close main program sequence and main plot
        footer = '};' + '\n' \
                 + '};'
        outfile.write(footer)





