Plot()
{
Outlier_Model("General", T(5), U(0,4), "t");
Curve("ShCal13","ShCal13.14c");
Sequence()
{
Boundary("Start occupation");
Phase()
{
R_date("NZ0222",823,40)  {Outlier("General",0.05);}  ;
R_date("NZ1167",410,86)  {Outlier("General",0.05);}  ;
R_date("NZ1644",775,59)  {Outlier("General",0.05);}  ;
R_date("NZ1645",777,59)  {Outlier("General",0.05);}  ;
R_date("NZ1647",687,58)  {Outlier("General",0.05);}  ;
R_date("NZ1648",681,58)  {Outlier("General",0.05);}  ;
R_date("NZ7170",490,30)  {Outlier("General",0.05);}  ;
R_date("NZ7171",475,30)  {Outlier("General",0.05);}  ;
R_date("NZ7780",481,46)  {Outlier("General",0.05);}  ;
R_date("NZ7812",1297,33)  {Outlier("General",0.05);}  ;
R_date("NZ7813",538,34)  {Outlier("General",0.05);}  ;
R_date("NZ7887",556,71)  {Outlier("General",0.05);}  ;
R_date("NZ7888",588,72)  {Outlier("General",0.05);}  ;
R_date("NZ7889",891,110)  {Outlier("General",0.05);}  ;
R_date("NZ7890",556,89)  {Outlier("General",0.05);}  ;
R_date("NZ7891",434,65)  {Outlier("General",0.05);}  ;
R_date("Wk2706",940,100)  {Outlier("General",0.05);}  ;
R_date("WK3721",510,45)  {Outlier("General",0.05);}  ;
};
Boundary();
};
};