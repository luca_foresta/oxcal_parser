# README 


## Table of Contents

- [Installation](#installation)
- [Examples](#examples)

## Installation

** Prerequisites: **

OxCal_parser is written in Python 2.7, so you need to have this installed on your system to run the program. Additionally it needs the Python Data Analysis Library 'Pandas'. The easiest way to install both Python 2.7 and the Pandas module together is to download and install Anaconda:
[https://conda.io/docs/user-guide/install/download.html]
which is available for Windows, macOS and Linux.

** Get OxCal_parser **

The best way to download the content of this repository is to use/create a git account and do a checkout. If you are not familiar with git, see below.

A simpler way to download the program is the following:

- from the main page, click on the file
- click on 'raw' on the upper right side of the page
- right click and 'save as'

You can do the same for other files inside the data folder if you wish to run the examples.

## Examples

Once you have downloaded (or checked out) the content of the repository, open a terminal, navigate to the folder where you have place OxCal_parser and type any of the following examples:

- python OxCal_parser.py data/Example_1_Iceland_Hofstadir.xlsx
- python OxCal_parser.py data/Example_3_Iceland_Reykjavik_Adalstraeti.xlsx

Each of them will create a corresponding text file in output (same name but with extention .txt) that you can open with any text editor. Copy paste the text in each fiel in the OxCal browser ([https://c14.arch.ox.ac.uk/oxcal/OxCal.html]) to run your analysis.



